<?php

namespace App\Http\Controllers;

use App\Article;
use App\Http\Requests;
use function back;
use function compact;
use Illuminate\Http\Request;
use App\Http\Requests\ArticleRequest;
use function view;


class ArticleController extends Controller

{
    public function index()
    {
        return view('home');
    }

    public function allNews()
    {
        $article = Article::orderBy('id', 'DESC')
            ->where('active', 1)
            ->get();
        return view('article.allnews', compact('article'));
    }

    public function sportsNews()
    {
        return view('article.sportnews');
    }

    public function socialNews()
    {
        return view('article.socialnews');
    }


    public function show($id)
    {
        $article = Article::findOrFail($id);

        return view('detail_article.show', compact('article'));
    }

    public function create()
    {
        return view('article.create_article');
    }

    public function store(ArticleRequest $request)
    {

//        //validation
//        $this->validate($request,[
//            'txtTitle'=>'required',
//            'txtBody' =>'required',
//            'cbActive' =>'boolean'
//        ]);

        $article = new Article();
        $article->title = $request->txtTitle;
        $article->body = $request->txtBody;
        $article->active = $request->cbActive;

        //alert success
        $request->session()->flash('message',
            '<div class="alert alert-success">Congratulations Insert</div>');

        $article->save();
        return back();

////        $article->create($request->all());
//        $article->create([
//            'title' => $request['title'],
//            'body' => $request['body'],
//            'active' => $request['active']
//        ]);


    }
}
