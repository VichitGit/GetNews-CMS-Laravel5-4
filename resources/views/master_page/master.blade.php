<!DOCTYPE html>
<html>
<head>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">

    <link rel="stylesheet" href="{{ asset('mycss/style.css') }}">



    <title>@yield('title')</title>
</head>
<body>
{{--master_page layout--}}
<div class="container">
    {{--navbar--}}
    @section('navbar')
        <div id="navbar-margin">
            <ul>
                <li><a class="active" href="{{'/'}}">Home</a></li>
                <li><a href="{{'/article'}}">All News</a></li>
                <li><a href="{{'/sports'}}">Sports News</a></li>
                <li><a href="{{'/socials'}}">Socials News</a></li>
                <div class="nav navbar-nav navbar-right" id="navbar-right">
                    <li><a href="/article/create">New Article</a></li>
                    <li><a href="#">Login</a></li>
                </div>
            </ul>

        </div>
    @show

    {{--menu-left--}}
    @section('menu-left')
        <div class="col-md-4">
            <div class="list-group">
                <a href="#" class="list-group-item active">Recent Upload</a>
                <a href="#" class="list-group-item">My article 1</a>

            </div>
        </div>
    @show

    {{--content--}}
    <div class="col-md-8">
        @yield('content')

    </div>

</div>
</body>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
</html>