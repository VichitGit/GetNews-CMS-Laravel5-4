@extends('master_page.master')
@section('content')
    <h1>Create New Article</h1>
    <p class="text-center">{!! session('message') !!}</p>

    <Form method="POST" action="{{ url('/article') }}">
        {{--Title--}}
        <div class="form-group">
            <label>Please input title</label>
            <input type="text" class="form-control" id="txtTitle" name="txtTitle" value="{{old('txtTitle')}}"
                   placeholder="My article...">
            <p class="text-danger">{{$errors->first('txtTitle')}}</p>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </div>

        {{--Body--}}
        <div class="form-group">
            <label>Please input description</label>
            <textarea class="form-control" rows="7" id="txtBody" name="txtBody"
                      placeholder="My Description...">{{old('txtBody')}}</textarea>
            <p class="text-danger">{{$errors->first('txtBody')}}</p>
        </div>

        {{--Body--}}
        <div class="form-group has-success">
            <label class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" value="1" checked id="cbActive" name="cbActive">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description">Active</span>
            </label>
        </div>
        <div class="form-group">
            <button class="btn btn-success" type="submit">Submit</button>
        </div>

    </Form>
@endsection