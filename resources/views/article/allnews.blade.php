@extends('master_page.master')
@section('content')
    <h1>All News</h1>
    <table class="table table-striped table-inverse">
        <thead>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Body</th>
            <th>Active</th>
            <th>View</th>
        </tr>
        </thead>
        <tbody>
        @foreach($article as $article)
            <tr>
                <th scope="row">{{ $article->id }}</th>
                <td>{{ $article->title }}</td>
                <td>{{ $article->body }}</td>
                <td>{{ $article->active }}</td>
                <td><a href="{{url($article->id)}}"><button type="button" class="btn btn-primary">View</button></a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop



